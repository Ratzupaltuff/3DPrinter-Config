#!/bin/bash
rsync -r pi@hazel.fritz.box:/home/pi/klipper_config /home/tuff/documents/ender3/klipper/config
rsync -r pi@hazel.fritz.box:/home/pi/klipper/out/klipper.bin /home/tuff/documents/ender3/klipper/firmware/klipper.bin
rsync -r pi@hazel.fritz.box:/home/pi/klipper/.config /home/tuff/documents/ender3/klipper/firmware/config
